package com.nextstep.webhybrid.NotyficationTools;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nextstep.webhybrid.NetworkTools.ICallback;
import com.nextstep.webhybrid.NetworkTools.RemoteActions.TmpRestService;

/**
 * Created by lodwr on 07.08.2017.
 */

public class NotyficationService extends Service {
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        clearNotyfications();
    }

    @Override
    public void onTaskRemoved (Intent rootIntent){
        stopSelf();
    }

    private void clearNotyfications(){
        NotificationManager nMgr = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }

    private void makeCall() {
        TmpRestService tmp = new TmpRestService(this);
        tmp.sendTmpRequest(new ICallback() {
            @Override
            public void callback(String response) {
                Log.d("ResponseService", response);
                Toast.makeText(getApplicationContext(), "Response from service!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void callbackError(VolleyError error) {

            }
        });
    }
}
