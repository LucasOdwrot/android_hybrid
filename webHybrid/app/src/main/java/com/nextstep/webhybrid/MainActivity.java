package com.nextstep.webhybrid;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nextstep.webhybrid.NetworkTools.ICallback;
import com.nextstep.webhybrid.NetworkTools.RemoteActions.TmpRestService;
import com.nextstep.webhybrid.NotyficationTools.NotyficationCenter;
import com.nextstep.webhybrid.NotyficationTools.SyncJobService;

public class MainActivity extends AppCompatActivity {

    private JobScheduler mJobScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mJobScheduler = (JobScheduler)
                getSystemService( Context.JOB_SCHEDULER_SERVICE );

        final Button button = (Button) findViewById(R.id.button);
        final Button startServiceBt = (Button) findViewById(R.id.startServiceBt);
        final Button stopServiceBt = (Button) findViewById(R.id.stopServiceBt);
        final Button serviceCallBt = (Button) findViewById(R.id.serverCallBt);

        final TextView textView = (TextView)findViewById(R.id.textView);

        final WebView myWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);

        //
        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.i("WebChrome", "|> onPermissionRequest");

                MainActivity.this.runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        Log.i("WebChrome", "|> onPermissionRequest run");
                        request.grant(request.getResources());
                    }// run
                });// MainActivity

            }// onPermissionRequest
        });// setWebChromeClient

//        page loaded from url
        myWebView.loadUrl("https://arty.name/localstorage.html");

        myWebView.addJavascriptInterface(new WebAppInterface(this, textView), "Android");
        myWebView.setWebViewClient(new WebViewClient());

//        String html = "<!doctype html><html lang=\"en\"> <head> <meta charset=\"UTF-8\"> <title>Document</title> </head> <body><script type=\"text/javascript\">function showAndroidToast(toast) { setWebViewText('Clicked from web view'); Android.showToast(toast);}function setWebViewText(text) { document.getElementById(\"demo\").innerHTML = text;}</script> <input type=\"button\" value=\"Say hello\" onClick=\"showAndroidToast('Hello Android!')\" /> <p id=\"demo\"></p> </body></html>";

        String mime = "text/html";
        String encoding = "utf-8";

        myWebView.getSettings().setJavaScriptEnabled(true);
//        myWebView.loadDataWithBaseURL(null, html, mime, encoding, null);



        final NotyficationCenter nc = new NotyficationCenter(this);

        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                textView.setText("Text changed from native button");
                myWebView.loadUrl("javascript:setWebViewText(\"Text changed from native button\")");
                nc.createNotyfication();
            }
        });

        startServiceBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), NotyficationService.class);
//                startService(intent);
                runJobScheduler();
            }
        });

        stopServiceBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), NotyficationService.class);
//                stopService(intent);
                mJobScheduler.cancelAll();
            }
        });

        serviceCallBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TmpRestService tmp = new TmpRestService(getApplicationContext());
                textView.setText("");
                tmp.sendTmpRequest(new ICallback() {
                    @Override
                    public void callback(String response) {
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(), "Response!", Toast.LENGTH_SHORT).show();
                        textView.setText(response);

                    }

                    @Override
                    public void callbackError(VolleyError error) {

                    }
                });
            }
        });


    }

    private void runJobScheduler() {
        mJobScheduler.cancelAll();

        JobInfo.Builder builder = new JobInfo.Builder(1,
                new ComponentName(getPackageName(),
                        SyncJobService.class.getName()));

        builder.setPeriodic(3000);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

        if( mJobScheduler.schedule( builder.build() ) <= 0 ) {
            Toast.makeText(getApplicationContext(), "There was a problem while starting job service", Toast.LENGTH_LONG).show();
        }
    }
}
