package com.nextstep.webhybrid.NetworkTools;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by lodwr on 19.04.2017.
 */

public class NetworkManager {

    private static Context mContext;
    private static NetworkManager mInstance;
    private static RequestQueue mReqQueue;

    public static final String TMP_URL = "https://jsonplaceholder.typicode.com/posts/1";

    private NetworkManager(Context context) {
        mContext = context;
        mReqQueue = getRequestQueue();
    }

    public static synchronized NetworkManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkManager(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mReqQueue == null) {
            mReqQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mReqQueue;
    }

    public void restTest(){
        // Instantiate the RequestQueue.
        Log.d("Rest test", "Http request");

        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url ="http://www.google.com";


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("Rest test", "Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Rest test", "Error");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    public void sendRequest(Request req) {
        getRequestQueue().add(req);
    }

    public static String addParamToUrl(String url, String key, String value) {
        if(!url.contains("?")) return url + "?" + key + "=" + value;
        else return url + "&" + key + "=" + value;
    }

    public static void logReqErrorData(VolleyError error) {
        if(error.networkResponse != null){
            Log.d("Error", "Code: " + error.networkResponse.statusCode);
            Log.d("Error", "Data: " + error.networkResponse.data);
            Log.d("Error", "Headers: " + error.networkResponse.headers);
        }

        Log.d("Error", "Message: " + error.getMessage());
    }
}
