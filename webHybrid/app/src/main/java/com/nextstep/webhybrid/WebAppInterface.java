package com.nextstep.webhybrid;

import android.app.Activity;
import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by lodwr on 02.08.2017.
 */

public class WebAppInterface {
    Context mContext;
    TextView mTextView;
    /** Instantiate the interface and set the context */
    WebAppInterface(Context c, TextView textView) {
        mContext = c;
        this.mTextView = textView;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(final String toast) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
                mTextView.setText("Text changed from web view!");
            }
        });
    }
}