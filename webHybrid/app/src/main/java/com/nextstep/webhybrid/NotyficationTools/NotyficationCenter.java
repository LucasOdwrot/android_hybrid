package com.nextstep.webhybrid.NotyficationTools;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import com.nextstep.webhybrid.MainActivity;
import com.nextstep.webhybrid.R;

/**
 * Created by lodwr on 07.08.2017.
 */

public class NotyficationCenter  {
    private Context mContext;
    private NotificationCompat.Builder mBuilder;

    public static  final int NOTYFICATION_ID = 9856731;

    public NotyficationCenter(Context context) {
        this.mContext = context;
        mBuilder = new NotificationCompat.Builder(mContext);
    }

    public void createNotyfication(){

        mBuilder.setSmallIcon(R.drawable.logo);
        mBuilder.setContentTitle("Tmp notyfication");
        mBuilder.setContentText("Hello");

        // Creates an explicit intent for an Activity in your app
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,
                intent, 0);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);



        // notificationID allows you to update the notification later on.
        mNotificationManager.notify(NOTYFICATION_ID, mBuilder.build());
    }

}
