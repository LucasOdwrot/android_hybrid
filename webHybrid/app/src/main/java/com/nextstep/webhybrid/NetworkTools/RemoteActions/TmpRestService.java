package com.nextstep.webhybrid.NetworkTools.RemoteActions;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nextstep.webhybrid.NetworkTools.ICallback;
import com.nextstep.webhybrid.NetworkTools.NetworkManager;

/**
 * Created by lodwr on 16.08.2017.
 */

public class TmpRestService {
    private NetworkManager mNetworkManager;
    private Context mContext;

    public TmpRestService(Context context) {
        mNetworkManager = NetworkManager.getInstance(context);
        mContext = context;
    }

    public void sendTmpRequest(final ICallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NetworkManager.TMP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(callback != null) callback.callback(String.valueOf(response));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkManager.logReqErrorData(error);
                if (callback != null) callback.callbackError(error);
            }
        });

        mNetworkManager.sendRequest(stringRequest);
    }
}
