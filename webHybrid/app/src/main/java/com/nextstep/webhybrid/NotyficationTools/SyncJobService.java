package com.nextstep.webhybrid.NotyficationTools;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nextstep.webhybrid.NetworkTools.ICallback;
import com.nextstep.webhybrid.NetworkTools.RemoteActions.TmpRestService;

/**
 * Created by lodwr on 16.08.2017.
 */

public class SyncJobService extends JobService {
    private static final String TAG = "SyncService";
    private Handler mJobHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage( Message msg ) {
            TmpRestService tmp = new TmpRestService(getApplicationContext());
            tmp.sendTmpRequest(new ICallback() {
                @Override
                public void callback(String response) {
                    Log.d("Response", response);
                    Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                    NotyficationCenter nc = new NotyficationCenter(getApplicationContext());
                    nc.createNotyfication();
                }

                @Override
                public void callbackError(VolleyError error) {

                }
            });

            jobFinished( (JobParameters) msg.obj, false );
            return true;
        }

    });

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d(TAG, "Job started....");
//        mJobHandler.sendMessage( Message.obtain( mJobHandler, 1, jobParameters));
        makeRestCall();
        jobFinished(jobParameters, false);

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        mJobHandler.removeMessages(1);
        return true;
    }

    private void makeRestCall() {
        TmpRestService tmp = new TmpRestService(getApplicationContext());
        tmp.sendTmpRequest(new ICallback() {
            @Override
            public void callback(String response) {
                Log.d("Response", response);
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                NotyficationCenter nc = new NotyficationCenter(getApplicationContext());
                nc.createNotyfication();
            }

            @Override
            public void callbackError(VolleyError error) {

            }
        });
    }
}
