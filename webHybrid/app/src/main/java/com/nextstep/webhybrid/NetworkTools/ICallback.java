package com.nextstep.webhybrid.NetworkTools;

import com.android.volley.VolleyError;

/**
 * Created by lodwr on 08.05.2017.
 */

public interface ICallback {
    public void callback(String respons);
    public void callbackError(VolleyError error);
}
